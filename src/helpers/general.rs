use crate::apis::call_request::call_gpt;
use crate::helpers::command_line::PrintCommand;
use crate::models::general::llm::Message;
use reqwest::Client;
use serde::de::DeserializeOwned;
use serde::Deserialize;
use std::fs;

const CODE_TEMPLATE_PATH: &str = "../ai_autogpt_web/src/code_template.rs";
const EXEC_MAIN_PATH: &str = "../ai_autogpt_web/src/main.rs";
const API_SCHEMA_PATH: &str = "./schemas/api_schema.json";

// Extend AI function to encourage specific output
pub fn extend_ai_function(ai_func: fn(&str) -> &'static str, func_input: &str) -> Message {
    let ai_function_str: &str = ai_func(func_input);

    // Extend the string to encourage only printing the output
    let msg: String = format!(
        "FUNCTION: {}
    INSTRUCTION: You are a function printer. You ONLY print the results of functions.
    Nothing else. No commentary. Here is the input to the function: {}.
    Print out what the function will return.
    ",
        ai_function_str, func_input
    );

    // Return message
    Message {
        role: String::from("system"),
        content: msg,
    }
}

// Performs call to LLM GPT
pub async fn ai_task_request(
    msg_context: String,
    agent_position: &str,
    agent_operation: &str,
    function_pass: for<'a> fn(&'a str) -> &'static str,
) -> String {
    // Extend AI function
    let extended_msg: Message = extend_ai_function(function_pass, &msg_context);

    // Print current status
    PrintCommand::AICall.print_agent_message(agent_position, agent_operation);

    // Get LLM response
    let llm_response_res: Result<String, Box<dyn std::error::Error + Send>> =
        call_gpt(vec![extended_msg.clone()]).await;

    // Return Success or try again
    match llm_response_res {
        Ok(llm_resp) => llm_resp,
        Err(e) => {
            eprintln!("Error on first call_gpt attempt {}", e);
            call_gpt(vec![extended_msg.clone()])
                .await
                .unwrap_or_else(|e_second_try| {
                    eprintln!("Error on second call_gpt attempt: {}", e_second_try);
                    String::from("There was an error in processing your request.")
                })
        }
    }
}

// Performs call to LLM GPT - Decoded
pub async fn ai_task_request_decoded<T: DeserializeOwned>(
    msg_context: String,
    agent_position: &str,
    agent_operation: &str,
    function_pass: for<'a> fn(&'a str) -> &'static str,
) -> T {
    let llm_response: String =
        ai_task_request(msg_context, agent_position, agent_operation, function_pass).await;

    // TODO Handle this expect more gracefully
    // Possible fix
    // let decoded_response: Result<T, Box<dyn std::error::Error>> = serde_json::from_str
    // (llm_response.as_str());
    // or it can be serde_json::Error
    let decoded_response: T = serde_json::from_str(llm_response.as_str())
        .expect("Failed to decode ai response from serde_json");

    decoded_response
}

pub async fn check_status_code(client: &Client, url: &str) -> Result<u16, reqwest::Error> {
    let response: reqwest::Response = client.get(url).send().await?;
    Ok(response.status().as_u16())
}

// Get Code Template
pub fn read_code_template_contents() -> String {
    let path: String = String::from(CODE_TEMPLATE_PATH);
    // TODO Handle this code without panicking
    fs::read_to_string(path).expect("Failed to read code template")
}

// Get Exec Main
pub fn read_exec_main_contents() -> String {
    let path: String = String::from(EXEC_MAIN_PATH);
    // TODO Handle this code without panicking
    fs::read_to_string(path).expect("Failed to read code template")
}

// Save New Backend Code
pub fn save_backend_code(contents: &String) {
    let path: String = String::from(EXEC_MAIN_PATH);
    // TODO Handle this without panicking
    fs::write(path, contents).expect("Failed to write to main.rs file");
}

pub fn save_api_endpoints(api_endpoints: &String) {
    let path: String = String::from(API_SCHEMA_PATH);
    // TODO Handle this without panicking
    fs::write(path, api_endpoints).expect("Failed to write API Endpoints to file");
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ai_functions::aifunc_managing::convert_user_input_to_goal;

    #[test]
    fn tests_extending_ai_function() {
        let extended_msg: Message =
            extend_ai_function(convert_user_input_to_goal, "dummy variable");

        dbg!(&extended_msg);

        assert_eq!(extended_msg.role, String::from("system"));
    }

    #[tokio::test]
    async fn tests_ai_task_request() {
        let ai_func_param: String =
            String::from("Build me a webserver for making weather forecast api requests");

        let res: String = ai_task_request(
            ai_func_param,
            "Managing Agent",
            "Defining user requirements",
            convert_user_input_to_goal,
        )
        .await;

        assert!(res.len() > 20);
    }
}
