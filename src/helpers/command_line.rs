use crossterm::{
    style::{Color, ResetColor, SetForegroundColor},
    ExecutableCommand,
};
use std::io::{stdin, stdout, Stdout};

// TODO Handle Duplicated Color sets and resets
// TODO Refactor the way a mut stdout variable was set in various functions, see confirm_safe_code() for fix

#[derive(PartialEq, Debug)]
pub enum PrintCommand {
    AICall,
    UnitTest,
    Issue,
}

impl PrintCommand {
    pub fn print_agent_message(&self, agent_pos: &str, agent_statement: &str) {
        let mut stdout: std::io::Stdout = stdout();

        // Decide on the print color
        let statement_color: Color = match self {
            Self::AICall => Color::Cyan,
            Self::UnitTest => Color::Magenta,
            Self::Issue => Color::Red,
        };

        // Print the agent statement in a specific color
        match stdout.execute(SetForegroundColor(Color::Green)) {
            Ok(_) => {
                print!("Agent: {}: ", agent_pos);
            }
            Err(e) => {
                eprintln!("Failed to get Foreground Color: {}", e);
            }
        }

        // Make selected color
        match stdout.execute(SetForegroundColor(statement_color)) {
            Ok(_) => {
                println!("{}", agent_statement);
            }
            Err(e) => {
                eprintln!("Failed to get Foreground Color: {}", e);
            }
        }

        match stdout.execute(ResetColor) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("Failed to reset color: {}", e);
            }
        }
    }
}

pub fn get_user_response(question: &str) -> String {
    let mut stdout: std::io::Stdout = stdout();

    // Print the question in a specific color
    match stdout.execute(SetForegroundColor(Color::Blue)) {
        Ok(_) => {}
        Err(e) => {
            eprintln!("Failed to get Foreground Color: {}", e);
        }
    }

    println!();
    println!("{}", question);

    // Reset Color
    match stdout.execute(ResetColor) {
        Ok(_) => {}
        Err(e) => {
            eprintln!("Failed to reset color: {}", e);
        }
    }

    // Read user input
    let mut user_input: String = String::new();

    let user_response: String = match stdin().read_line(&mut user_input) {
        Ok(_) => String::from(user_input.trim()),
        Err(e) => {
            eprintln!("Failed to read response: {}", e);
            return String::new();
        }
    };

    user_response
}

// Get user response that code is safe to execute
pub fn confirm_safe_code() -> bool {
    loop {
        // Print the question in specified color
        match Stdout::execute(&mut stdout(), SetForegroundColor(Color::DarkYellow)) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("Failed to set Foreground color: {}", e);
            }
        }

        // Print warning output
        let warning_output: &str = r#"
WARNING: You are about to run code written entirely by AI. 
Review your code and confirm you wish to continue.
        "#;
        println!("{}", warning_output);

        // Reset Color
        match Stdout::execute(&mut stdout(), ResetColor) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("Failed to reset foreground color: {}", e);
            }
        }

        // Present Options with different colors
        match Stdout::execute(&mut stdout(), SetForegroundColor(Color::Green)) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("Failed to set foreground color: {}", e);
            }
        }
        println!("[1] All good");
        match Stdout::execute(&mut stdout(), SetForegroundColor(Color::DarkRed)) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("Failed to set foreground color: {}", e);
            }
        }
        println!("[2] Lets stop this project");

        // Reset Color
        match Stdout::execute(&mut stdout(), ResetColor) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("Failed to reset foreground color: {}", e);
            }
        }

        // Read user input
        let mut human_response: String = String::new();

        let human_response: String = match stdin().read_line(&mut human_response) {
            Ok(_) => human_response.trim().to_lowercase(),
            Err(e) => {
                eprintln!("Failed to read response: {}", e);
                continue;
            }
        };

        match human_response.as_str() {
            "1" | "ok" | "y" => return true,
            "2" | "no" | "n" => return false,
            _ => {
                eprintln!("Invalid input. Please select '1' or '2'");
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tests_prints_agent_msg() {
        PrintCommand::AICall
            .print_agent_message("Managing Agent", "Testing testing, processing something!");
    }
}
