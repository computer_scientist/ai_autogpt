use crate::ai_functions::aifunc_architect::{print_project_scope, print_site_urls};
use crate::helpers::command_line::PrintCommand;
use crate::helpers::general::{ai_task_request_decoded, check_status_code};
use crate::models::agent_basic::basic_agent::{AgentState, BasicAgent};
use crate::models::agent_basic::basic_traits::BasicTraits;
use crate::models::agents::agent_traits::{FactSheet, ProjectScope, SpecialFunctions};

use async_trait::async_trait;
use reqwest::Client;
use std::time::Duration;

// Solutions Architect
#[derive(Debug)]
pub struct AgentSolutionArchitect {
    attributes: BasicAgent,
}

impl AgentSolutionArchitect {
    pub fn new() -> Self {
        let attributes: BasicAgent = BasicAgent {
            objective: String::from(
                "Gathers information and design solutions for website development",
            ),
            position: String::from("Solutions Architect"),
            state: AgentState::Discovery,
            memory: vec![], // TODO Figure if this will be as is or Vec::new() or Vec::from([])
        };

        Self { attributes }
    }

    async fn call_project_scope(&mut self, factsheet: &mut FactSheet) -> ProjectScope {
        let msg_context: String = factsheet.project_description.to_string();

        let ai_response: ProjectScope = ai_task_request_decoded::<ProjectScope>(
            msg_context,
            &self.attributes.position,
            get_function_string!(print_project_scope),
            print_project_scope,
        )
        .await;

        factsheet.project_scope = Some(ai_response);
        self.attributes.update_state(AgentState::Finished);

        ai_response
    }

    async fn call_determine_external_urls(
        &mut self,
        factsheet: &mut FactSheet,
        msg_context: String,
    ) {
        let ai_response: Vec<String> = ai_task_request_decoded::<Vec<String>>(
            msg_context,
            &self.attributes.position,
            get_function_string!(print_site_urls),
            print_site_urls,
        )
        .await;

        factsheet.external_urls = Some(ai_response);
        self.attributes.state = AgentState::UnitTesting;
    }
}

#[async_trait]
impl SpecialFunctions for AgentSolutionArchitect {
    fn get_attributes_from_agent(&self) -> &BasicAgent {
        &self.attributes
    }

    async fn execute(
        &mut self,
        fact_sheet: &mut FactSheet,
    ) -> Result<(), Box<dyn std::error::Error>> {
        // !!! WARNING - BE CAREFUL OF INFINITE LOOPS
        // TODO Figure how to get an alternative solution that would surely prevent infinite loops.
        while self.attributes.state != AgentState::Finished {
            match self.attributes.state {
                AgentState::Discovery => {
                    let project_scope: ProjectScope = self.call_project_scope(fact_sheet).await;

                    // Confirm if external urls
                    if project_scope.is_external_urls_required {
                        self.call_determine_external_urls(
                            fact_sheet,
                            fact_sheet.project_description.clone(),
                        )
                        .await;
                        self.attributes.state = AgentState::UnitTesting;
                    }
                }
                AgentState::UnitTesting => {
                    let mut exclude_urls: Vec<String> = vec![];

                    let client: Client =
                        Client::builder().timeout(Duration::from_secs(5)).build()?;

                    // Defining urls to check
                    // Note: This is a good alternative to writing as_ref.expect("...")
                    let urls: &Vec<String> =
                        fact_sheet.external_urls.as_ref().ok_or_else(|| {
                            Box::new(std::io::Error::new(
                                std::io::ErrorKind::NotFound,
                                "No URL object on fact sheet",
                            )) as Box<dyn std::error::Error>
                        })?;

                    // Find faulty urls
                    for url in urls {
                        let endpoint_str: String = format!("Testing URL Endpoint: {}", url);
                        PrintCommand::UnitTest.print_agent_message(
                            self.attributes.position.as_str(),
                            endpoint_str.as_str(),
                        );

                        // Perform URL Test
                        match check_status_code(&client, url).await {
                            Ok(status_code) => {
                                if status_code != 200 {
                                    exclude_urls.push(url.clone())
                                }
                            }
                            Err(e) => eprintln!("Error checking {}: {}", url, e),
                        }
                    }

                    // Exclude any faulty urls
                    if !exclude_urls.is_empty() {
                        if let Some(current_urls) = fact_sheet.external_urls.as_ref() {
                            let new_urls: Vec<String> = current_urls
                                .iter()
                                .filter(|url| !exclude_urls.contains(url))
                                .cloned()
                                .collect();

                            fact_sheet.external_urls = Some(new_urls);
                        }
                    }

                    // Confirm done
                    self.attributes.state = AgentState::Finished;
                }

                // Default to finish state
                _ => {
                    self.attributes.state = AgentState::Finished;
                }
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn tests_solution_architect() {
        let mut agent: AgentSolutionArchitect = AgentSolutionArchitect::new();

        let mut factsheet: FactSheet = FactSheet {
            project_description: String::from(
                "Build a full stack website with user login and logout that shows the current \
                UTC time",
            ),
            project_scope: None,
            external_urls: None,
            backend_code: None,
            api_endpoint_schema: None,
        };

        // Alternative to .await.expect in this case where there is an explicit panic call.
        match agent.execute(&mut factsheet).await {
            Ok(_) => {}
            Err(e) => panic!("Failed to execute Solutions Architect Agent: {}", e),
        }

        assert_ne!(factsheet.project_scope, None);
        assert!(factsheet.external_urls.is_some());

        dbg!(factsheet);
    }
}
