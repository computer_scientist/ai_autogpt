use crate::models::general::llm::{APIResponse, ChatCompletion, Message};
use dotenv::dotenv;
use reqwest::header::{HeaderMap, HeaderValue};
use reqwest::{Client, Response};
use std::env;
use std::fmt::format;

// Call Large Language Model
pub async fn call_gpt(messages: Vec<Message>) -> Result<String, Box<dyn std::error::Error + Send>> {
    dotenv().ok();

    // Extract API Key information
    let api_key: Result<String, Box<dyn std::error::Error + Send>> =
        env::var("OPEN_AI_KEY").map_err(|e| -> Box<dyn std::error::Error + Send> { Box::new(e) });

    let api_org: Result<String, Box<dyn std::error::Error + Send>> =
        env::var("OPEN_AI_ORG").map_err(|e| -> Box<dyn std::error::Error + Send> { Box::new(e) });

    // Confirm endpoint
    let url: &str = "https://api.openai.com/v1/chat/completions";

    // Create headers
    let mut headers: HeaderMap = HeaderMap::new();

    // Create api key header
    let api_key: String = api_key?;

    headers.insert(
        "authorization",
        HeaderValue::from_str(&format!("Bearer {}", api_key))
            .map_err(|e| -> Box<dyn std::error::Error + Send> { Box::new(e) })?,
    );

    // Create Open AI Org header
    let api_org: String = api_org?;

    headers.insert(
        "OpenAI-Organization",
        HeaderValue::from_str(api_org.as_str())
            .map_err(|e| -> Box<dyn std::error::Error + Send> { Box::new(e) })?,
    );

    // Create client
    let client: Client = Client::builder()
        .default_headers(headers)
        .build()
        .map_err(|e| -> Box<dyn std::error::Error + Send> { Box::new(e) })?;

    // Create chat completion
    // For initial testing use gpt-3.5-turbo for cost-efficient testing
    // Then change to gpt-4-turbo for final testing
    let chat_completion: ChatCompletion = ChatCompletion {
        model: String::from("gpt-3.5-turbo"),
        messages,
        temperature: 0.1,
    };

    // Troubleshooting
    // let res_raw: Result<Response, Box<dyn std::error::Error + Send>> = client
    //     .post(url)
    //     .json(&chat_completion)
    //     .send()
    //     .await
    //     .map_err(|e| -> Box<dyn std::error::Error + Send> { Box::new(e) });
    //
    // let res_raw: Response = res_raw?;
    //
    // let res_raw_text: Result<String, Box<dyn std::error::Error + Send>> = res_raw
    //     .text()
    //     .await
    //     .map_err(|e| -> Box<dyn std::error::Error + Send> { Box::new(e) });
    //
    // let _ = dbg!(res_raw_text);

    // Extract API Response
    let res: APIResponse = client
        .post(url)
        .json(&chat_completion)
        .send()
        .await
        .map_err(|e| -> Box<dyn std::error::Error + Send> { Box::new(e) })?
        .json()
        .await
        .map_err(|e| -> Box<dyn std::error::Error + Send> { Box::new(e) })?;

    Ok(res.choices[0].message.content.clone())
}

#[cfg(test)]
mod tests {
    use super::*;

    // To test use `cargo test -- --nocapture
    #[tokio::test]
    async fn tests_call_to_openai() {
        let message: Message = Message {
            role: String::from("user"),
            content: String::from("Hi there, this is a test. Give me a very short response."),
        };

        let messages: Vec<Message> = vec![message];

        let res: Result<String, Box<dyn std::error::Error + Send>> = call_gpt(messages).await;

        match res {
            Ok(res_str) => {
                dbg!(res_str);
                assert!(true);
            }
            Err(_) => {
                assert!(false);
            }
        }
    }
}
