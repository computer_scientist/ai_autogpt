# AutoGPT

## About the Project

This Rust-based project is featured in the Udemy course, "Build an AutoGPT Code Writing AI Tool
With Rust and GPT-4." It works in conjunction with a companion project, AutoGPT Web, which provides
the templates from which the agents operate.

The project is structured around three agents—a manager, an architect, and a backend developer.
The operation begins with an initial prompt that defines the project requirements. These agents
then collaborate like a typical software development team to design, test, fix, and ultimately
deliver a fully functional stack application. This includes the development of frontend endpoints,
backend logic, and the integration of services to ensure seamless operation.

## Limitations

While utilizing GPT-4-turbo, the project has successfully managed multiple test scenarios across
various projects. However, some issues were encountered, such as unwanted markdown block tags at
the beginning and end of generated code, occasionally leading to build failures. In some cases,
the system was able to autonomously correct these bugs, though not consistently.

GPT-3.5-turbo performed adequately with basic project requirements but struggled with more complex
demands. The GPT-3.5-turbo-16k variant showed slight improvements but still fell short in handling
intricate projects effectively.

As of the latest update, enhancing the model's performance might be achievable by utilizing a more
structured and thoughtful template, potentially switching to Hyper from Actix, and refining the
prompts used in the ai_functions for better clarity.

## Further reading

The project falls in line with the following research `Large Language Models as Tool Makers` that can be found at:
https://arxiv.org/abs/2305.17126

A video review of that research paper can be found at:
https://www.youtube.com/watch?v=qWI1AJ2nSDY

Finally, a good read `Chain-of-Thought Prompting Elicits Reasoning in Large Language Models`:
https://arxiv.org/abs/2201.11903
